package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7758676644158652494L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		
//		retrieving of branding data
		ServletContext srvContext = getServletContext();
		String brand = srvContext.getInitParameter("branding");
		
//		retrieving of first name from system properties
		String firstName = System.getProperty("firstName");
		
//		retrieving of last name from HttpSession
		HttpSession session = req.getSession();
		String lastName = session.getAttribute("lastName").toString();
		
//		retrieving of email from ServletContext
		String email = srvContext.getAttribute("email").toString();
		
//		retrieving contact from url param
		String contact = req.getParameter("contact");
		
		out.println(
				"<div style=\"display:flex; justify-content:center\">"
				+ "<div><h1>"+ brand +"</h1>"
				+ "<p>First Name: "+ firstName +"</p>"
				+ "<p>Last Name: "+ lastName +"</p>"
				+ "<p>Contact: "+ contact +"</p>"
				+ "<p>Email: "+ email +"</p></div>"
				+ "</div>"
				);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" DetailsServlet has been destroy. ");
		System.out.println("******************************************");
	}
}
