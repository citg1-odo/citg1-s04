package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" UserServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
//		saving firstName via System Properties
		String firstName = req.getParameter("firstName");
		System.getProperties().put("firstName", firstName);
		
//		saving lastName via HttpSession
		String lastName = req.getParameter("lastName");
		HttpSession session = req.getSession();
		session.setAttribute("lastName", lastName);
		
//		saving email via Servlet Context
		String email = req.getParameter("email");
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", email);
		
//		saving contact via url rewriting
		String contact = req.getParameter("contact");
		res.sendRedirect("details?contact="+contact);
	}
	
	public void destroy(){
		System.out.println("******************************************");
		System.out.println(" UserServlet has been destroy. ");
		System.out.println("******************************************");
	}
	
}
